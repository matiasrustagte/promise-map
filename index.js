class PromiseMap extends Map {
  async inParallel (cb) {
    if (typeof cb !== 'function') {
      throw Error('Bad param: "PromiseMap.inParallel" requires callback');
    }
    await Promise.all(Array.from(this.values()).map(cb));
  }

  async inSeries (cb) {
    if (typeof cb !== 'function') {
      throw Error('Bad param: "PromiseMap.inSeries" requires callback');
    }
    for (const obj of this.values()) {
      await cb(obj);
    }
  }
}

module.exports = PromiseMap;
